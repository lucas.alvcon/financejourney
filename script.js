document.addEventListener('DOMContentLoaded', () => {
    const downloadButton = document.querySelector('.download-button');

    setInterval(() => {
        downloadButton.classList.toggle('pulse');
    }, 1000);
});
